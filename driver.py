import re
import math


def read():
    array = []
    with open("trainEN.txt","r") as line:
        for text in line:
            for w in text.split():
                word = re.sub('[^a-zA-Z]+', '', w)
                array.append(word)

    wordSet = set(array)
    return wordSet


def readFrench():
    arrayFR = []
    with open("trainFR.txt","r") as line:
        for text in line:
            for w in text.split():
                word = re.sub('[^a-zA-Z]+', '', w)
                arrayFR.append(word)

    wordSetFR = set(arrayFR)
    return wordSetFR


def readFinish():
    arrayFIN = []
    with open("trainOT.txt","r") as line:
        for text in line:
            for w in text.split():
                word = re.sub('[^a-zA-Z]+', '', w)
                arrayFIN.append(word)
    wordSetFin = set(arrayFIN)
    return wordSetFin

#get letter count
def getLetterCount(wordset,char):
    wordcount = 0
    for i in wordset:
        for letter in i:
            if letter == char:
                wordcount = wordcount + 1
    return wordcount + 13

#get totalletter count
def getTotalLetterCount(wordset):
    wordcount = 0
    for i in wordset:
        for letter in i:
                wordcount = wordcount + 1
    return wordcount + 13

#get cond prob
def getprob(wordset,char1,char2):
    wordcount = 0
    for i in wordset:
        for index, letter in enumerate(i):
            if index+1 < len(i):
                if i[index] == char1 and i[index+1] == char2:
                    wordcount = wordcount + 1;

    if wordcount == 0:
        return 0.5/getLetterCount(wordset,char1)
    else:
        return (wordcount + 0.5) /getLetterCount(wordset,char1)


def getprobuni(wordset, char1):
    wordcount = 0
    for i in wordset:
        for index, letter in enumerate(i):
            if index + 1 < len(i):
                if i[index] == char1:
                    wordcount = wordcount + 1;
    if wordcount == 0:
        return 0.5/getLetterCount(wordset,char1)
    else:
        return (wordcount + 0.5)/ getTotalLetterCount(wordset)


def main():
    char = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
                'r','s','t','u','v','w','x','y','z']


    sampleWordSet = ['fun','test','fungus']
    languages = ['ENG','FR']
    #get words
    wordset = read()
    wordsetFR = readFrench();
    wordsetFIN = readFinish();

    fileen_bi = open("bigramEN.txt", "w")
    filefr_bi = open("bigramFR.txt", "w")
    filefinn_bi = open("bigramOT.txt", "w")

    print("Dumping Bigram for EN")
    for i in char:
        for z in char:
            bigramtexten2 = "P(" + str(z) + '|' + str( i) + ") = " + str(getprob(wordset, z, i))
            fileen_bi.write(bigramtexten2 + "\n")

    print("Dumping Bigram for FR")
    for i in char:
        for z in char:
            bigramtextfr2 = "P(" + str(z) + '|' + str(i) + ") = " + str(getprob(wordsetFR, z, i))
            filefr_bi.write(bigramtextfr2 + "\n")

    print("Dumping Bigram for OT")
    for i in char:
        for z in char:
            bigramtextfin2 = "P(" + str(z) + '|' + str(i) + ") = " + str(getprob(wordsetFIN, z, i))
            filefinn_bi.write(bigramtextfin2 + "\n")

    fileen_uni = open("unigramEN.txt", "w")
    filefr_uni = open("unigramFR.txt", "w")
    filefinn_uni = open("unigramOT.txt", "w")

    print("Dumping unigram for EN")
    for t in char:
        unigramtexten2 = "P(" + str(t)+ ") = " + str(getprobuni(wordset,t))
        fileen_uni.write(unigramtexten2 + "\n")

    print("Dumping unigram for FR")
    for t in char:
        unigramtextfr2 = "P(" + str(t)+ ") = " + str(getprobuni(wordsetFR,t))
        filefr_uni.write(unigramtextfr2 + "\n")

    print("Dumping unigram for OT")
    for t in char:
        unigramtextfin2 = "P(" + str(t) + ") = " + str(getprobuni(wordsetFIN,t))
        filefinn_uni.write(unigramtextfin2 + "\n")


    #
    # print(len(wordset))
    # print(len(wordsetFR))
    # print(len(wordsetFIN))

    engtotal = getTotalLetterCount(wordset)
    frtotal = getTotalLetterCount(wordsetFR)
    fintotal = getTotalLetterCount(wordsetFIN)

    # print(engtotal)
    # print(frtotal)
    # print(fintotal)

    # print(getLetterCount(wordset,'f'))
    # print(getprob(wordset,'f','u'))
    # var =  raw_input("Please enter something:")
    # print("You entered " + str(var))


    with open("inputsentences.txt", "r") as line:
        for index, text in enumerate(line):
            engprobbi = 0
            engprobuni = 0
            frprobbi = 0

            frprobuni = 0
            finprobbi = 0
            finprobuni = 0
            print("writing file ", str(index + 1), "....." )
            filename = 'out' + str(index + 1) + ".txt"
            print(filename)
            file = open(filename, "w")
            file.write(text)
            parsedWords = re.sub('[^a-zA-Z]+', '', text)
            file.write('\n')
            file.write("UNIGRAM MODEL: "+ '\n')
            for index, letter in enumerate(parsedWords):
                engprobuni = engprobuni + math.log10(getprobuni(wordset, parsedWords[index]))
                frprobuni = frprobuni + math.log10(getprobuni(wordsetFR, parsedWords[index]))
                finprobuni = finprobuni + math.log10(getprobuni(wordsetFIN, parsedWords[index]))

                unigramtext = "UNIGRAM: " + str(parsedWords[index])

                file.write(str(unigramtext) + '\n')

                unigramen = "ENGLISH: P(" + str(parsedWords[index]) + ") = " + str(engprobuni)
                file.write("".join(unigramen) + '\n')

                unigramfr = "FRENCH: P(" + str(parsedWords[index]) + ") = " + str(frprobuni)
                file.write("".join(unigramfr) + '\n')

                unigramfin = "FINNISH: P(" + str(parsedWords[index]) + ") = " + str(finprobuni)
                file.write("".join(unigramfin) + '\n')

                file.write( '\n')
                file.write('\n')

            if engprobuni > frprobuni and engprobuni > finprobuni:
                file.write("According to the unigram model, the sentence is English"+ '\n')
            if finprobuni > engprobuni and finprobuni > frprobuni:
                file.write( "According to the unigram model, the sentence is Finnish"+ '\n')
            if frprobuni > finprobuni and frprobuni > engprobuni:
                file.write( "According to the unigram model, the sentence is French"+ '\n')

            file.write("----------------------"+ '\n')
            file.write("BIGRAM MODEL"+ '\n')
            file.write('\n')

            for index, letter in enumerate(parsedWords):
                if index+1 < len(parsedWords) and parsedWords[index+1] != '.':
                    #run prob calculation
                    engprobbi = engprobbi + math.log10(getprob(wordset,parsedWords[index],parsedWords[index+1]))
                    frprobbi = frprobbi + math.log10(getprob(wordsetFR,parsedWords[index],parsedWords[index+1]))
                    finprobbi = finprobbi + math.log10(getprob(wordsetFIN,parsedWords[index],parsedWords[index+1]))

                    bigramtext = "BIGRAM: " + str(parsedWords[index]) + str(parsedWords[index+1])
                    file.write(str(bigramtext)  + '\n')


                    # print("ENGLISH:P(",parsedWords[index],parsedWords[index+1],") = ", str(engprobbi))
                    bigramtexteng2 = "ENGLISH:P(" + str(parsedWords[index]) +'|'+ parsedWords[index+1],") = " + str(engprobbi)

                    file.write("".join(bigramtexteng2) + '\n')

                    bigramtextfr2 = "FRENCH:P(" + str(parsedWords[index]) +'|'+ str(parsedWords[index + 1]) + ") = " + str(frprobbi)
                    file.write("".join(bigramtextfr2) + '\n')

                    # print("FINNISH: P(" , parsedWords[index], parsedWords[index + 1], ") = ", str(finprobbi))
                    bigramtextfin2 = "FINNISH: P(" + str(parsedWords[index]) +'|'+ str(parsedWords[index + 1]) + ") = " + str(finprobbi)

                    file.write("".join(bigramtextfin2) + '\n')
                    file.write( '\n')
                    file.write('\n')

            if engprobbi > finprobbi and engprobbi > frprobbi:
                file.write("According to the bigram model, the sentence is English"  + '\n')
            if finprobbi > engprobbi and finprobbi > frprobbi:
                file.write( "According to the bigram model, the sentence is Finnish" + '\n')
            if frprobbi > finprobbi and frprobbi > engprobbi:
                file.write( "According to the bigram model, the sentence is French" + '\n')
    print("Done")


if __name__ == "__main__":
    main()
