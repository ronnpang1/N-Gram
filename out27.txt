Mitä perheellesi kuuluu?

UNIGRAM MODEL: 
UNIGRAM: M
ENGLISH: P(M) = -2.72800685732
FRENCH: P(M) = -3.01617796796
FINNISH: P(M) = -2.84668202672


UNIGRAM: i
ENGLISH: P(i) = -3.85613064256
FRENCH: P(i) = -4.10175498776
FINNISH: P(i) = -3.86379756322


UNIGRAM: t
ENGLISH: P(t) = -5.11911174958
FRENCH: P(t) = -5.39983863798
FINNISH: P(t) = -4.94008398794


UNIGRAM: p
ENGLISH: P(p) = -6.7387701079
FRENCH: P(p) = -6.94412482942
FINNISH: P(p) = -6.59324948913


UNIGRAM: e
ENGLISH: P(e) = -7.73234200024
FRENCH: P(e) = -7.84719866173
FINNISH: P(e) = -7.71792362855


UNIGRAM: r
ENGLISH: P(r) = -8.95752859227
FRENCH: P(r) = -8.98630783815
FINNISH: P(r) = -9.31957982575


UNIGRAM: h
ENGLISH: P(h) = -10.5399537276
FRENCH: P(h) = -10.9321982732
FINNISH: P(h) = -10.954787838


UNIGRAM: e
ENGLISH: P(e) = -11.5335256199
FRENCH: P(e) = -11.8352721055
FINNISH: P(e) = -12.0794619774


UNIGRAM: e
ENGLISH: P(e) = -12.5270975123
FRENCH: P(e) = -12.7383459378
FINNISH: P(e) = -13.2041361168


UNIGRAM: l
ENGLISH: P(l) = -13.8428925417
FRENCH: P(l) = -14.1093596843
FINNISH: P(l) = -14.4141183997


UNIGRAM: l
ENGLISH: P(l) = -15.1586875711
FRENCH: P(l) = -15.4803734307
FINNISH: P(l) = -15.6241006825


UNIGRAM: e
ENGLISH: P(e) = -16.1522594634
FRENCH: P(e) = -16.383447263
FINNISH: P(e) = -16.7487748219


UNIGRAM: s
ENGLISH: P(s) = -17.470103437
FRENCH: P(s) = -17.7356661314
FINNISH: P(s) = -17.8665431952


UNIGRAM: i
ENGLISH: P(i) = -18.5982272222
FRENCH: P(i) = -18.8212431512
FINNISH: P(i) = -18.8836587317


UNIGRAM: k
ENGLISH: P(k) = -20.7016632378
FRENCH: P(k) = -22.2295315841
FINNISH: P(k) = -20.1150577007


UNIGRAM: u
ENGLISH: P(u) = -22.2002032779
FRENCH: P(u) = -23.5988026951
FINNISH: P(u) = -21.381473492


UNIGRAM: u
ENGLISH: P(u) = -23.6987433181
FRENCH: P(u) = -24.9680738061
FINNISH: P(u) = -22.6478892832


UNIGRAM: l
ENGLISH: P(l) = -25.0145383475
FRENCH: P(l) = -26.3390875525
FINNISH: P(l) = -23.8578715661


UNIGRAM: u
ENGLISH: P(u) = -26.5130783876
FRENCH: P(u) = -27.7083586635
FINNISH: P(u) = -25.1242873573


UNIGRAM: u
ENGLISH: P(u) = -28.0116184278
FRENCH: P(u) = -29.0776297745
FINNISH: P(u) = -26.3907031486


According to the unigram model, the sentence is Finnish
----------------------
BIGRAM MODEL

BIGRAM: Mi
ENGLISH:P(M|i) = -1.16422625777
FRENCH:P(M|i) = -0.912297988779
FINNISH: P(M|i) = -0.458211721945


BIGRAM: it
ENGLISH:P(i|t) = -2.27461583838
FRENCH:P(i|t) = -1.74107831917
FINNISH: P(i|t) = -1.54465429493


BIGRAM: tp
ENGLISH:P(t|p) = -5.03893336568
FRENCH:P(t|p) = -5.34244288284
FINNISH: P(t|p) = -4.31351601548


BIGRAM: pe
ENGLISH:P(p|e) = -5.75032985815
FRENCH:P(p|e) = -6.09186998196
FINNISH: P(p|e) = -5.25014603411


BIGRAM: er
ENGLISH:P(e|r) = -6.57337578407
FRENCH:P(e|r) = -6.99023321375
FINNISH: P(e|r) = -6.55439861059


BIGRAM: rh
ENGLISH:P(r|h) = -8.98483703177
FRENCH:P(r|h) = -9.73973169006
FINNISH: P(r|h) = -8.13184912965


BIGRAM: he
ENGLISH:P(h|e) = -9.5690786647
FRENCH:P(h|e) = -10.2468855942
FINNISH: P(h|e) = -8.97258310766


BIGRAM: ee
ENGLISH:P(e|e) = -11.0450057084
FRENCH:P(e|e) = -11.7621303324
FINNISH: P(e|e) = -10.0610671566


BIGRAM: el
ENGLISH:P(e|l) = -12.3541047099
FRENCH:P(e|l) = -13.2576160383
FINNISH: P(e|l) = -10.8748911526


BIGRAM: ll
ENGLISH:P(l|l) = -13.3128592182
FRENCH:P(l|l) = -14.1664698057
FINNISH: P(l|l) = -11.5083513768


BIGRAM: le
ENGLISH:P(l|e) = -14.0162916558
FRENCH:P(l|e) = -14.6607074303
FINNISH: P(l|e) = -12.3152563284


BIGRAM: es
ENGLISH:P(e|s) = -14.8934006573
FRENCH:P(e|s) = -15.4504511184
FINNISH: P(e|s) = -13.4241562455


BIGRAM: si
ENGLISH:P(s|i) = -16.1694597682
FRENCH:P(s|i) = -16.7009318598
FINNISH: P(s|i) = -14.1377466942


BIGRAM: ik
ENGLISH:P(i|k) = -18.1773642338
FRENCH:P(i|k) = -20.3638784741
FINNISH: P(i|k) = -15.3489410898


BIGRAM: ku
ENGLISH:P(k|u) = -20.4064665045
FRENCH:P(k|u) = -22.1222856663
FINNISH: P(k|u) = -16.3215471416


BIGRAM: uu
ENGLISH:P(u|u) = -23.958322955
FRENCH:P(u|u) = -25.0219602854
FINNISH: P(u|u) = -17.3126799109


BIGRAM: ul
ENGLISH:P(u|l) = -24.9903514117
FRENCH:P(u|l) = -26.1919302833
FINNISH: P(u|l) = -18.3980641267


BIGRAM: lu
ENGLISH:P(l|u) = -26.4742957392
FRENCH:P(l|u) = -27.6558697961
FINNISH: P(l|u) = -19.6778891445


BIGRAM: uu
ENGLISH:P(u|u) = -30.0261521897
FRENCH:P(u|u) = -30.5555444152
FINNISH: P(u|u) = -20.6690219138


According to the bigram model, the sentence is Finnish
